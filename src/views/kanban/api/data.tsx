import { v4 as uuid } from 'uuid';
import ITask from '../Interfaces/ITask';
import IColumn from '../Interfaces/IColumn';

export const tasksData: ITask[] = [
  { 
    id: 1,
    avatar: 'https://clck.ru/qrjuh',
    author: 'Lehamedtg',
    title: 'Название задачи длиной не более 50 символов текста',
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                  Corrupti, consequuntur! Molestias recusandae, sapiente voluptas modi doloremque 
                  impedit eveniet.`,
    trainee: 'N0by',
    topic: 'React + TS',
    date: '31.01.2022 14:41',
    column: 'В работе',
    status: 'Активна',
    type: 'Разработка',
    priority: 'Средний',
    dedline: '2022-08-07',
    grade: '7',
  },
  { 
    id: 2,
    avatar: 'https://clck.ru/qrjuh',
    author: 'singleton11',
    title: 'Название задачи длиной не более 50 символов текста',
    description: `Vivamus elementum in nibh vitae eleifend. Quisque lorem orci, mattis sit amet neque quis, 
                  tempor lobortis justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
                  posuere cubilia curae; Sed tristique, neque a tempus euismod, turpis metus convallis 
                  tellus, non iaculis dolor nulla sed nisi. Etiam ornare sodales tellus.`,
    trainee: 'Dany',
    topic: 'Backend',
    date: '07.06.2022 15:55',
    column: 'На проверке',
    status: 'Активна',
    type: 'Тестирование',
    priority: 'Высокий',
    dedline: '2022-06-20',
    grade: '5.5',
  },
  { 
    id: 3,
    avatar: 'https://clck.ru/qrjuh',
    author: 'singleton11',
    title: 'Название задачи длиной не более 50 символов текста',
    description: `In vulputate lobortis tellus, ut ultrices massa consectetur vitae. 
                  Aenean tincidunt, arcu mattis porttitor convallis, tortor quam varius magna, 
                  a consequat tortor tellus nec ante. Sed lacinia luctus dui, vel vulputate lectus 
                  dapibus sed. Donec purus dui, tristique vel tortor et, gravida vestibulum sem. 
                  Ut quis lorem dui. Pellentesque quis tortor tempor, lacinia orci et, ornare augue. 
                  Nulla in velit ante. Etiam congue malesuada lectus vitae sodales. Suspendisse justo diam, 
                  congue in mauris vitae, pretium placerat elit. Quisque nisi neque, faucibus vitae leo non, 
                  facilisis malesuada nulla. Morbi vehicula turpis at neque ultrices malesuada. 
                  Mauris nisi justo, congue quis orci vitae, porta vulputate odio.`,
    trainee: 'Nikita',
    topic: 'Backend',
    date: '13.06.2022 19:25',
    column: 'В работе',
    status: 'Приостановлена',
    type: 'Алгоритм',
    priority: 'Низкий',
    dedline: '2022-03-06',
    grade: '4',
  },
];

const columnsData: IColumn[] = [
  { id: uuid(), name: 'К изучению', items: tasksData },
  { id: uuid(), name: 'В работе', items: [] },
  { id: uuid(), name: 'На проверке', items: [] },
  { id: uuid(), name: 'Готовые', items: [] },
];

export default columnsData;