import { Row } from 'antd';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';
import { useState, Dispatch } from 'react';
import columnsData from '../api/data';
import OuterColumnItem from '../OuterColumnItem/OuterColumnItem';
import Modal from '../../../components/Modal/Modal';
import ITask from '../Interfaces/ITask';
import IColumn from '../Interfaces/IColumn';
import './Board.css';

const onDragEnd = (result: DropResult, columns: any, setColumns: Dispatch<[]>) => {
  if (!result.destination) return;
  const { source, destination } = result;

  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: { ...sourceColumn, items: sourceItems },
      [destination.droppableId]: { ...destColumn, items: destItems },
    });
  } else {
    const column = columns[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: { ...column, items: copiedItems },
    });
  }
};

export default function Board() {
  const [columns, setColumns] = useState<IColumn[]>(columnsData);
  const [activeModal, setActiveModal] = useState<boolean>(false);
  const [modalItem, setModalItem] = useState({} as ITask);

  const getOuterColumnItems = () => {
    const result: JSX.Element[] = [];
    Object.entries(columns).map(([columnId, column]) => 
      result.push(<OuterColumnItem key={columnId} columnId={columnId} column={column} 
        openModal={setActiveModal} setItem={setModalItem} />),
    );
    return result;
  };

  return (
    <>
      <Row gutter={[16, 16]} align="middle" className="kanban__row">
        <DragDropContext onDragEnd={result => onDragEnd(result, columns, setColumns)} 
          children={getOuterColumnItems()} />
      </Row>

      <Modal active={activeModal} setActive={setActiveModal} item={modalItem} />
    </>
  );
}
