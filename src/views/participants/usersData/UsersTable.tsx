import { Col, Row } from 'antd';
import { FC } from 'react';

const colStylesCustomization = {
    // full width === 24 cols;
    minWidth: {
      '568px': {widthInCols: 24},
      '768px': {widthInCols: 20, horizontalPaddingInCols: 2},
    }
};
  
const colStyles = {
    xs: { span: colStylesCustomization.minWidth['568px'].widthInCols },
    md: {
        span: colStylesCustomization.minWidth['768px'].widthInCols,
        offset: colStylesCustomization.minWidth['768px'].horizontalPaddingInCols,
    },
};

export const UsersTable: FC<IUsersTableProps> = ({users}) => {

return (
  <Row>
    <Col {...colStyles}>
      {users()}
    </Col>
 </Row>
 )
}

interface IUsersTableProps {
    users :any
}