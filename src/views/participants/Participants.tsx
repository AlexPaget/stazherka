import './participants.css';
import { Props } from './types';

import React, { FC } from 'react';
import { Content } from 'antd/lib/layout/layout';
import Title from 'antd/lib/typography/Title';
import { Col, Divider, Row } from 'antd';
import Users from './usersData/Users';
import { UsersTable } from './usersData/UsersTable';




const Participants: FC<Props> = () => {

  return (
    <Content className="Participants__content">
       <Title>Участники</Title>
       <Divider />
       <UsersTable users={Users}/>
    </Content>
  );
};

export default Participants;
