import { Dispatch, useState } from 'react';
import ModalForm from '../../views/kanban/ModalForm/ModalForm';
import ModalView from '../../views/kanban/ModalView/ModalView';
import ITask from '../../views/kanban/Interfaces/ITask';
import ModalAlarm from '../../views/kanban/ModalAlarm/ModalAlarm';
import './Modal.css';

export default function Modal({ active, setActive, item }: IProps) {
  const [type, setType] = useState('read');

  if (type === 'read' && localStorage.getItem('create') !== null) {
    setType('write');
  }

  const closeModal = () => {
    if (type === 'write' && localStorage.getItem('new') !== null) {
      setType('alarm');
    } else {
      setActive(false);
      setType('read');
      localStorage.clear();
    }
  };

  const View = <ModalView closeModal={setActive} item={item} setType={setType} />;
  const Edit = <ModalForm closeModal={setActive} item={item} setType={setType} />;
  const Alarm = <ModalAlarm closeModal={setActive} setType={setType} />;

  return (
    <div className={active ? 'modal active' : 'modal'} onClick={closeModal}>
      <div className="modal__content" onClick={e => e.stopPropagation()}>

        { type === 'read' && View }
        { type === 'write' && Edit }
        { type === 'alarm' && Alarm }

        <button className="modal__close" onClick={closeModal} >&times;</button>
      </div>
    </div>
  );
}

interface IProps {
  active: boolean;
  setActive: Dispatch<boolean>;
  item: ITask;
}